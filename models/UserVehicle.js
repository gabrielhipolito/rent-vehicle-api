module.exports = (sequelize, Sequelize, user, vehicle) => {
    const UserVehicle = sequelize.define("user_vehicles", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        rented_date: Sequelize.DATE,
        still_rented: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true,
        },
        user_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'users',
                key: 'id',
            },
        },
        vehicle_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'vehicles',
                key: 'id',
            }
        }

    });
    // user.belongsToMany(vehicle, { through: 'user_vehicle', foreignKey: 'user_id', constraints: true});
    // vehicle.belongsToMany(user, { through: 'user_vehicle', foreignKey: 'vehicle_id', constraints: true});

    return UserVehicle;
};
