module.exports = (sequelize, Sequelize) => {
  const Vehicle = sequelize.define("vehicle", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true,
    },
    model: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    color: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    reserved: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    price: {
      type: Sequelize.DOUBLE,
      allowNull: false,
    },
  });

  return Vehicle;
};
