const sequelize = require("../config/db.js");
const Sequelize = require("sequelize");
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.vehicleModel = require("./Vehicle")(sequelize, Sequelize);
db.userModel = require("./User")(sequelize, Sequelize);
db.userVehicleModel = require("./UserVehicle")(
  sequelize,
  Sequelize,
  db.userModel,
  db.vehicleModel
);

module.exports = db;
