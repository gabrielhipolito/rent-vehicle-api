const VehicleRoute = require("./vehicle.routes");
const UserRoute = require("./user.routes");

module.exports = (app) => {
  VehicleRoute(app, "/veiculo");
  UserRoute(app, "/usuario");
};
