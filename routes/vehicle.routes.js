const vehicle = require("../controllers/vehicle.controller");

module.exports = (app, baseRoute) => {
  app.post(`${baseRoute}/criar_frota`, vehicle.create);
  app.get(`${baseRoute}/listar`, vehicle.getVehicles);
  app.post(`${baseRoute}/alugar`, vehicle.rentVehicle)
  app.delete(`${baseRoute}/alugar`, vehicle.quitVehicle)
  app.get(`${baseRoute}/alugados`, vehicle.getRentedVehicles)


};
