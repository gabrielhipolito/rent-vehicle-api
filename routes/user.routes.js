const user = require("../controllers/user.controller");

module.exports = (app, baseRoute) => {
  app.post(`${baseRoute}/cadastrar`, user.signUp);
  app.get(`${baseRoute}/entrar`, user.signIn);
  app.delete(`${baseRoute}`, user.logout);
};
