const bcrypt = require("bcryptjs");
const models = require("../models");
const UserModel = models.userModel;

class UserController {
  static signUp = async (req, res) => {
    try {
      const salt = await bcrypt.genSaltSync(10);
      const hashPassword = await bcrypt.hashSync(req.body.password, salt);

      let user = await UserModel.create({
        username: req.body.username,
        email: req.body.email,
        password: hashPassword,
        logged: true,
      });

      user = await user.save();
      if (!user) {
        return res.status(200).send({
          success: false,
          status: 404,
          message: "Algo de ruim aconteceu e não encontramos nada",
        });
      }
      res.status(200).send({
        data: {userId: user.id, username: user.username, logged: user.logged },
        success: true,
        status: 200,
        message: "Cadastro realizado com sucesso",
      });
    } catch (error) {
      console.log(error);
      return res.status(500).send({
        success: false,
        message: "E-mail ou usuário já estão sendo utilizados",
        errors: error,
        status: 500,
      });
    }
  };
  static signIn = async (req, res) => {
    try {
      const { username, password } = req.query;
      if (!username || !password) {
        return res
          .status(500)
          .send({
            success:false,
            status: 500,
            message:"Não é possível autenticar sem um usuário ou senha"
          });
      }

      const user = await UserModel.findOne({ where: { username } });
      if (bcrypt.compareSync(password, user.password)) {
        user.logged = true;
        await user.save();
        res.status(200).send({
          data: {userId: user.id, username: user.username },
          success: true,
          status: 200,
          message: "Autenticação feita com sucesso",
        });
      } else {
        return res.status(200).send({
          success: false,
          status: 200,
          message: "Senha inválida",
        });
      }
    } catch (error) {
      console.log(error);
      return res.status(500).send({
        success: false,
        message: "Usuário não existe",
        errors: error,
        status: 500,
      });
    }
  };
  static logout = async (req, res) => {
    try {
      const { username } = req.query;
      if (!username) {
        return res
            .status(500)
            .send({
              success:false,
              status: 500,
              message:"Você já não está logado"
            });
      }

      const user = await UserModel.findOne({ where: { username } });

        user.logged = false;
        await user.save();

        return res.status(200).send({
          success: false,
          status: 200,
          message: "Usuário foi deslogado",
        });
    } catch (error) {
      console.log(error);
      return res.status(500).send({
        success: false,
        message: "Não foi possível deslogar o usuário",
        errors: error,
        status: 500,
      });
    }
  };

}

module.exports = UserController;
