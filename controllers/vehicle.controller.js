const models = require("../models");
const VehicleModel = models.vehicleModel;
const UserVehicleModel = models.userVehicleModel
const sequelize = require("../config/db.js");


class VehicleController {
    static create = async (req, res) => {
        try {

            const vehicles = [
                {model: "Gol", reserved: false, color: "Amarelo", price: 10000},
                {model: "Uno", reserved: false, color: "Verdade", price: 10000},
                {model: "Palio", reserved: false, color: "Amarelo", price: 10000},
                {model: "Fox", reserved: false, color: "Vermelho", price: 10000},
                {model: "Siena", reserved: false, color: "Azul", price: 10000},
                {model: "Voyage", reserved: false, color: "Preto", price: 10000},
                {model: "HB20", reserved: false, color: "Preto Fosco", price: 10000},
                {model: "Celta", reserved: false, color: "Branco", price: 10000},
                {model: "Peugeot", reserved: false, color: "Prata", price: 10000},

            ];

            vehicles.forEach(async vehicle  => {
                const vehicleDetails = await VehicleModel.create({
                    model: vehicle.model,
                    reserved: vehicle.reserved,
                    color: vehicle.color,
                    price: vehicle.price
                });
                await vehicleDetails.save();
            })

            res.status(200).send({
                success: true,
                status: 200,
                message: "O cadastro do veículo foi feito com sucesso",
            });
        } catch (error) {
            console.log(error);
            return res.status(500).send({
                success: false,
                message: "Não foi possível cadastrar o veículo",
                errors: error,
                status: 500,
            });
        }
    }
    static getVehicles = async (req, res) => {
        try {
            let vehicleDetails = await VehicleModel.findAll();
            if (!vehicleDetails) {
                return res.status(200).send({
                    success: false,
                    status: 404,
                    message: 'Não foi possível encontrar veículos'
                });
            }

            vehicleDetails = vehicleDetails.filter(vehicle => !vehicle.reserved)
            res.status(200).send({
                success: true,
                data: vehicleDetails,
                status: 200,
                message: 'Veículos encontrados com sucesso'
            });
        } catch (error) {
            console.log(error)
            return res.status(500).send({
                message: 'Não foi possível encontrar veículos',
                errors: error,
                status: 500
            });
        }
    };
    static rentVehicle = async (req, res) => {
        try {
            const {vehicleId, userId} = req.body;
            const vehicle = await VehicleModel.findByPk(vehicleId);
            let userVehicle = await UserVehicleModel.findAll({where: {user_id: userId, vehicle_id: vehicleId}});

            if (!vehicleId || !vehicle || !userId) {
                return res.status(500).send({
                    success: false,
                    status: 500,
                    message: "Não foi possível encontrar esse veículo",
                });
            }

            if (vehicle.reserved) {
                return res.status(200).send({
                    success: false,
                    status: 200,
                    message: "Esse veículo já está alugado"
                });
            }
            if (userVehicle.length > 0) {
                userVehicle[0].rented_date = new Date();
                userVehicle[0].still_rented = true;
                await userVehicle[0].save().then(
                    async () => {
                        vehicle.reserved = true;
                        await vehicle.save();
                    })
            } else {
                userVehicle = await UserVehicleModel.create({
                    user_id: userId,
                    vehicle_id: vehicleId,
                    rented_date: new Date(),
                    still_rented: true
                });
                await userVehicle.save().then(
                    async () => {
                        vehicle.reserved = true;
                        await vehicle.save();
                    })
            }
            return res.status(200).send({
                success: true,
                status: 200,
                message: "Veículo alugado com sucesso",
            });
        } catch (error) {
            console.log(error);
            return res.status(500).send({
                success: false,
                message: "Não foi possível alugar esse veículo",
                errors: error,
                status: 500,
            });
        }
    };
    static quitVehicle = async (req, res) => {
        try {
            const {userId, vehicleId} = req.query;
            const userVehicle = await UserVehicleModel.findAll({where: {user_id: userId, vehicle_id: vehicleId}});
            const vehicle = await VehicleModel.findByPk(vehicleId);

            if (!userVehicle || !userId || !vehicleId) {
                return res.status(500).send({
                    success: false,
                    status: 500,
                    message: "Não foi possível encontrar esse veículo",
                });
            }

            userVehicle[0].still_rented = false;
            await userVehicle[0].save();

            const reservedVehicles = await UserVehicleModel.findAll({where: {vehicle_id: vehicleId}});
            if(reservedVehicles.length > 0) {
                let isReserved = false;
                reservedVehicles.forEach(vehicle => {
                    if(vehicle.still_rented) {
                        isReserved = true
                    }
                })

                if(!isReserved) vehicle.reserved = false;
                await vehicle.save();
            } else {
               vehicle.reserved = false;
                await vehicle.save();
            }



            return res.status(200).send({
                success: true,
                status: 200,
                message: "O veículo não está mais alugado",
            });
        } catch (error) {
            console.log(error);
            return res.status(500).send({
                success: false,
                message: "Ocorreu um erro ao deixar o veículo",
                errors: error,
                status: 500,
            });
        }
    };
    static getRentedVehicles = async (req, res) => {
        try {
            const userVehicleDetails =
                await sequelize.query("select * from user_vehicles uv join vehicles v on v.id = uv.vehicle_id WHERE uv.user_id = " + req.query.userId, {
                    model: UserVehicleModel,
                    mapToModel: true
                })
            if (!userVehicleDetails) {
                return res.status(200).send({
                    success: false,
                    status: 404,
                    message: 'Não foi possível encontrar veículos NO SEU NOME'
                });
            }
            res.status(200).send({
                success: true,
                data: userVehicleDetails,
                status: 200,
                message: 'Veículos encontrados com sucesso'
            });
        } catch (error) {
            console.log(error)
            return res.status(500).send({
                message: 'Não foi possível encontrar veículos',
                errors: error,
                status: 500
            });
        }
    };
}

module.exports = VehicleController
