# Aluguel de Veículos
Essa é a API que fornece dados para o front do Aluguel de Veículos.

# Especificações

- O banco utilizado foi SQLite
- A comunicação é feita atráves da arquitetura REST

## Tecnologias
- NodeJs 14.16
## Inicialização da Aplicação

Dê o clone do projeto através do comando
```
git clone https://github.com/HipolitoDeveloper/rent-vehicle-api.git
```

Após isso, instale as dependências com
```
npm i
```

Por fim a API está pronta para ser inicializada, caso você queira alterar a porta de inicialização, mude na linha 18 do arquivo server.js.

E então pode rodar
```
npm start
```

Como a API utiliza SQLite, você precisará criar o schema do banco. Para fazer isso de forma automática
acesse a rota http://localhost:SUA_PORTA/sync, pelo navegador ou por um software de requisição (Postman ou Nodemon)

Se o retorno for SUCCESS:true, você está pronto para configurar o [Front](https://github.com/HipolitoDeveloper/rent-vehicle-front)

Para conseguir ter uma experiência completa faça um POST na rota http://localhost:SUA_PORTA/veiculo/criar_frota, para criar a frota que poderá ser utilizada de teste no front.

<!-- CONTACT -->
## Contatos

Gabriel Hipólito - hipolitodeveloper@gmail.com

LinkedIn: [https://www.linkedin.com/in/gabriel-hipolito-b26ba215a/](https://www.linkedin.com/in/gabriel-hipolito-b26ba215a/)

Link do Projeto API: [Aluguel de Carros - API](https://github.com/HipolitoDeveloper/rent-vehicle-api)
Link do Projeto FRONT: [Aluguel de Carros - Front](https://github.com/HipolitoDeveloper/rent-vehicle-front)


## Autor

Gabriel Hipólito