const express = require("express");
const bodyParser = require("body-parser");
const app = express();

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, DELETE");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type, Authorization"
  );
  next();
});

require("./routes/routes")(app);
const PORT = process.env.PORT || 7000;

app.listen(PORT, () => {
  console.log(`Servidor rodando na porta... ${PORT}.`);
});

app.get("/sync", async (req, res) => {
  const database = require("./config/db");
  try {
    const result = await database.sync();
    res.status(200).send({
      status: 200,
      message: "O banco foi sincronizado com sucesso",
    });
  } catch (error) {
    res.status(500).send({
      status: 500,
      message: "Não foi possível sincronizar o banco",
    });
    console.log(error);
  }
});
